package by.bsuir.myfirstplayground;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import static android.graphics.Paint.Align.CENTER;

public class MainActivity extends AppCompatActivity {

    EditText editSize;
    Button btnOk;
    class DrawView extends View {

        Paint p;
        Rect rect;

        public DrawView(Context context) {
            super(context);
            p = new Paint();
            rect = new Rect();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            p.setColor(Color.BLACK);
            p.setStrokeWidth(5);
            int width = canvas.getWidth();
            int height = canvas.getHeight();
            int Oy = height - width;
            canvas.drawLine(0, Oy - Oy / 2, width, Oy - Oy / 2, p);
            canvas.drawLine(0, height - Oy / 2, width, height - Oy / 2, p);
            int size = Integer.parseInt(editSize.getText().toString());
            float side = width / size;

            for(int i = 1; i < size; i++){
                canvas.drawLine(side * i, Oy - Oy / 2, side * i, height - Oy / 2,  p);
                canvas.drawLine(0,Oy - Oy / 2 + side * i, width, Oy - Oy / 2 + side * i, p);
            }

            //draw circles
            float radius, y;
            radius = side / 2;
            y = Oy / 2 + radius;
            for(int i = 0; i < size; i++) {
                for(int j = 0;  j < size; j++) {
                    canvas.drawCircle(radius + side * j, y, radius, p);
                }
                y = y + side;
            }
//            //Text
//            p.setTextSize(60);
//            canvas.drawText("Tap to return", 400, 180, p);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Help button
        final AlertDialog aldlgHelp = new AlertDialog.Builder(MainActivity.this).create();
        aldlgHelp.setTitle("About");
        aldlgHelp.setMessage(getResources().getString(R.string.help_text));
        aldlgHelp.setButton(AlertDialog.BUTTON_NEUTRAL, "CLOSE",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        ImageButton btnHelp;
        btnHelp = (ImageButton) findViewById(R.id.imageButton);
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aldlgHelp.show();
            }
        });
        editSize = (EditText) findViewById(R.id.editSize);
        btnOk = (Button) findViewById(R.id.btnOk);

        editSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (editSize.length() != 0) {
                    btnOk.setEnabled(true);
                }
                else {
                    btnOk.setEnabled(false);
                }
                try {
                    if (Integer.parseInt(editSize.getText().toString()) > 20) {
                        editSize.setText("20");
                        Toast.makeText(MainActivity.this,
                                R.string.max_size_toast,
                                Toast.LENGTH_SHORT).show();
                    }
                }
                catch (NumberFormatException e) {
                    btnOk.setEnabled(false);
                }
            }
        });

        View.OnClickListener oclBtnOk = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.playgrlayout);
                View v;
                v = (View) findViewById(R.id.view);
                v.setClickable(true);
                setContentView(new DrawView(v.getContext()));
            }
        };
        btnOk.setOnClickListener(oclBtnOk);

//        View playgroundView;
//        playgroundView = findViewById(R.id.view);
//        View.OnClickListener oclTapToReturn = new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setContentView(R.layout.activity_main);
//            }
//        };
//        if (View.generateViewId() == R.layout.playgrlayout) {
//            playgroundView.setOnClickListener(oclTapToReturn);
//        }
    }
}


